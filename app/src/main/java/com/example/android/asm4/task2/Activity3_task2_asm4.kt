package com.example.android.asm4.task2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.example.fresher2022_kotlin_quanglv8b.R

class Activity3_task2_asm4 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_activity3_task2_asm4)
        var tenmonan = intent.getStringExtra("ten").toString()
        var motamonan = intent.getStringExtra("mota").toString()
        var giamonam = intent.getStringExtra("gia")!!.toFloat()
        var hinhanhmonan = intent.getStringExtra("hinhanh")!!.toInt()

        var ten = findViewById<TextView>(R.id.textView2)
        var mota = findViewById<TextView>(R.id.textView3)
        var gia = findViewById<TextView>(R.id.textView4)
        var hinhanh : ImageView = findViewById<ImageView>(R.id.hinhanh)
        var capnhat = findViewById<Button>(R.id.button)


        ten.text = "Tên món ăn: $tenmonan"
        mota.text = "Mô tả: $motamonan"
        gia.text = "Giá: $giamonam đ"
        hinhanh.setImageResource(hinhanhmonan)

        capnhat.setOnClickListener {
            val intent : Intent = Intent(this,Activity1_task2_asm4::class.java)
            intent.putExtra("upten", tenmonan)
            intent.putExtra("upmota",motamonan)
            intent.putExtra("upgia", giamonam)
            startActivity(intent)
        }
    }
}