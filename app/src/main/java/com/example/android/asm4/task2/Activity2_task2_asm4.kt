package com.example.android.asm4.task2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ListView
import com.example.fresher2022_kotlin_quanglv8b.R

class Activity2_task2_asm4 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_activity2_task2_asm4)

        var arraymonan : ArrayList<MonAn> = ArrayList<MonAn>()
        arraymonan.add(MonAn(1,"Tôm", "Tôm hùm", 200f, R.drawable.img_1))
        arraymonan.add(MonAn(2,"Bún","Bún bò Nam Định", 30f, R.drawable.img_2))
        arraymonan.add(MonAn(3,"Bún đậu","Bún đậu mắm tôm",40f, R.drawable.img_3))
        arraymonan.add(MonAn(4,"Cá","Cá biển tươi sống",300f, R.drawable.img_4))
        arraymonan.add(MonAn(5,"Lẩu","Lẩu thái thập cẩm",350f, R.drawable.img_5))


        val ma : Int = intent.getStringExtra("ma1").toString()!!.toInt()
        val ten = intent.getStringExtra("ten1").toString()
        val mota = intent.getStringExtra("mota1").toString()
        val gia = intent.getStringExtra("gia1").toString().toFloat()
        Log.i("tag", ma!!.toString())

        val monan = MonAn(ma,ten,mota,gia,R.drawable.img_1)
        arraymonan.add(monan)
//
        // <sử dụng listview
        val listview = findViewById<ListView>(R.id.listview)
        listview.adapter = ListViewSP(this@Activity2_task2_asm4, arraymonan)

        listview.setOnItemClickListener { parent, view, position, id ->
            var monan = arraymonan[position] as MonAn
            var intent : Intent = Intent(this, Activity3_task2_asm4::class.java)
            intent.putExtra("ten", monan.ten )
            intent.putExtra("mota", monan.mota )
            intent.putExtra("gia", monan.gia.toString() )
            intent.putExtra("hinhanh", monan.hinhanh.toString() )
            startActivity(intent)
        }
        // sử dụng listview>

        //<sử dụng Recyclerview
//        var adapter = RecyclerAdaptera(arraymonan)
//        var linearLayoutManager = LinearLayoutManager(this)
//        var recyclerView = findViewById(R.id.itemView) as RecyclerView
//        recyclerView?.adapter = adapter
//        recyclerView?.layoutManager = linearLayoutManager
//
//
//        adapter.itemClick = { monan ->
//            val intent:Intent = Intent(this,Activity3_task2_asm4::class.java)
//            intent.putExtra("ma", monan.ma)
//            intent.putExtra("ten", monan.ten)
//            intent.putExtra("mota", monan.mota)
//            intent.putExtra("gia", monan.gia)
//            startActivity(intent)
//        }
        // Recyclerview>
    }
}