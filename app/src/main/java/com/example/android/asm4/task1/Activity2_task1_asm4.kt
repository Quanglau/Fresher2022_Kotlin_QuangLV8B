package com.example.android.asm4.task1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import com.example.fresher2022_kotlin_quanglv8b.R

class Activity2_task1_asm4 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.constraintlayout_activity2_task1_asm4)

        val button2 = findViewById<Button>(R.id.button2)
        val button3 = findViewById<Button>(R.id.button3)
        val taikhoan = intent.getStringExtra("tai khoan").toString()
        val hoten = intent.getStringExtra("ho ten").toString()
        val tuoi = intent.getStringExtra("tuoi")
        Log.d("tag", hoten)


        val text1 = findViewById<TextView>(R.id.textView2)
        val text2 = findViewById<TextView>(R.id.textView4)
        val text3 = findViewById<TextView>(R.id.textView6)
        text1.text = "Xin chào $hoten"
        text2.text = "Tài khoản $taikhoan"
        text3.text = "Tuổi $tuoi"

        button2.setOnClickListener {
            val intent: Intent = Intent(this, Activity1_task1_asm4::class.java)
            startActivity(intent)
        }

        button3.setOnClickListener {
            val intent: Intent = Intent(this, Activity1_task1_asm4::class.java)
            finish()
        }
    }
}