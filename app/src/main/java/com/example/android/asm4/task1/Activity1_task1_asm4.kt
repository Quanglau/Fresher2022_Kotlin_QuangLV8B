package com.example.android.asm4.task1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import com.example.fresher2022_kotlin_quanglv8b.R

class Activity1_task1_asm4 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.constraintlayout_activity1_task1_asm4)
        val button = findViewById<Button>(R.id.button)

        button.setOnClickListener{

            val taikhoan = findViewById<EditText>(R.id.editTextTextPersonName).text.toString()
            val hoten = findViewById<EditText>(R.id.editTextTextPersonName2).text.toString()
            val tuoi = findViewById<EditText>(R.id.editTextTextPersonName3).text.toString()
            val gioitinh = findViewById<Spinner>(R.id.spiner)
//            if (gioitinh != null) {
//               // val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, gioitinh)
//                val adapter = arrayOf("nam", "nữ")
//                adapter.also { gioitinh.adapter = it }

            Log.d("tag", "main: $hoten")
            val intent: Intent = Intent(this, Activity2_task1_asm4::class.java)
            intent.putExtra("tai khoan", taikhoan)
            intent.putExtra("ho ten", hoten)
            intent.putExtra("tuoi", tuoi)
            startActivity(intent)
        }
    }
}