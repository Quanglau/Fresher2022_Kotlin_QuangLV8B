package com.example.android.asm4.task2

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.example.fresher2022_kotlin_quanglv8b.R

class ListViewSP (
    var context: Context,
    var mangmonan:ArrayList<MonAn>): BaseAdapter() {

    class ViewHolder(row: View) {
        var textviewmamonan: TextView
        var textviewtenmonan: TextView
        var textviewmotamonan: TextView
        var textviewgiamonan: TextView
        var imageviewmonan: ImageView

        init {
            textviewmamonan = row.findViewById(R.id.mamonan) as TextView
            textviewtenmonan = row.findViewById(R.id.tenmonan) as TextView
            textviewmotamonan = row.findViewById(R.id.motamonan) as TextView
            textviewgiamonan = row.findViewById(R.id.giamonan) as TextView
            imageviewmonan = row.findViewById(R.id.imageviewhinhmonan) as ImageView

        }
    }

    override fun getCount(): Int {
        return mangmonan.size
    }

    override fun getItem(position: Int): Any {
        return mangmonan[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        var view: View?
        var viewholder: ViewHolder
        if (convertView == null) {
            var layoutinflater: LayoutInflater = LayoutInflater.from(context)
            view = layoutinflater.inflate(R.layout.dong_monan, null)
            viewholder = ViewHolder(view)
            view.tag = viewholder
        } else {
            view = convertView
            viewholder = convertView.tag as ViewHolder
        }
        var monan: MonAn = getItem(position) as MonAn
        viewholder.textviewmamonan.text = monan.ma.toString()
        viewholder.textviewtenmonan.text = monan.ten
        viewholder.textviewmotamonan.text = monan.mota
        viewholder.textviewgiamonan.text = monan.gia.toString()
        viewholder.imageviewmonan.setImageResource(monan.hinhanh)
        return view as View
    }
}