package com.example.android.asm4.task2

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.fresher2022_kotlin_quanglv8b.R

class RecyclerViewSP (private val arraymonan: ArrayList<MonAn>)
    : RecyclerView.Adapter<RecyclerViewSP.ViewHolder>() {

    var itemClick : ((MonAn) -> Unit)? = null

    inner class ViewHolder(row: View): RecyclerView.ViewHolder(row) {
        var textviewmamonan : TextView?=null
        var textviewtenmonan: TextView?=null
        var textviewmotamonan : TextView?=null
        var textviewgiamonan : TextView?=null
        var imageviewmonan: ImageView?=null

        init {
            textviewmamonan = row.findViewById(R.id.mamonan) as TextView
            textviewtenmonan = row.findViewById(R.id.tenmonan) as TextView
            textviewmotamonan = row.findViewById(R.id.motamonan) as TextView
            textviewgiamonan = row.findViewById(R.id.giamonan) as TextView
            imageviewmonan = row.findViewById(R.id.imageviewhinhmonan) as ImageView

            row.setOnClickListener{

                itemClick?.invoke(arraymonan[adapterPosition])
            }
        }
    }

    override fun getItemCount(): Int {
        return arraymonan.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var monan: MonAn = arraymonan[position]
        holder.textviewmamonan!!.text = monan.ma.toString()
        //String().format("Ma: %d", monan.ma.toString())
        holder.textviewtenmonan!!.text = monan.ten.toString()
        //String().format("Ten: %s", monan.ten)
        holder.textviewmotamonan!!.text =monan.mota.toString()
        //String().format("Mo ta : %s", monan.mota)
        holder.textviewgiamonan!!.text = monan.gia.toString()
        //String().format("Gia: %f", monan.gia.toString())
        holder.imageviewmonan?.setImageResource(monan.hinhanh)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var context: Context = parent.context
        var inflater = LayoutInflater.from(context)
        var  productView: View = inflater.inflate(R.layout.dong_monan,parent)
        return ViewHolder(productView)
    }
}