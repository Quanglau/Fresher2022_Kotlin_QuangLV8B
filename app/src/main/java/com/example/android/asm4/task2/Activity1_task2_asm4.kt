package com.example.android.asm4.task2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import com.example.fresher2022_kotlin_quanglv8b.R

class Activity1_task2_asm4 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_activity1_task2_asm4)

        val button = findViewById<Button>(R.id.buttonXacNhan)

        button.setOnClickListener {
            val intent: Intent = Intent(this, Activity2_task2_asm4::class.java)
            val ma : Int = findViewById<EditText>(R.id.EditTextMa).text.toString()!!.toInt()
            Log.i("tag", ma!!.toString())
            val ten = findViewById<EditText>(R.id.EditTextTen).text
            val mota = findViewById<EditText>(R.id.EditTextMoTa).text
            val gia = findViewById<EditText>(R.id.EditTextGia).text



            intent.putExtra("ma1", ma.toString())
            intent.putExtra("ten1", ten.toString() )
            intent.putExtra("mota1", mota.toString() )
            intent.putExtra("gia1", gia.toString() )
            startActivity(intent)
        }
    }
}