package com.example.android.asm2_3.task2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.fresher2022_kotlin_quanglv8b.R

class Activity2_task2_asm2_3 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_activity2_task2_asm23)
        val button = findViewById<Button>(R.id.button2)
        val num1 = intent.getStringExtra("number1")
        val num2 = intent.getStringExtra("number2")

        button.setOnClickListener{
            var sum = num1.toString().toInt() + num2.toString().toInt()
            val intent: Intent = Intent(this, Activity1_task2_asm2_3::class.java)
            intent.putExtra("sum", sum.toString())
            startActivity(intent)
        }
    }
}