package com.example.android.asm2_3.task1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.fresher2022_kotlin_quanglv8b.R

class Activity1_task1_asm2_3 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_activity1_task1_asm23)
        val button = findViewById<Button>(R.id.move_screen)
        button.setOnClickListener() {
            val intent: Intent = Intent(this, Activity2_task1_asm2_3::class.java)
            val stringList = arrayListOf(
                "Hello!", "Hi!", "Salut!", "Hallo!",
                "Ciao!", "Ahoj!", "YAHsahs!", "Bog!", "Hej ! ", " Czesc!", " Ní! ",
                " Kon'nichiwa! ", " Annyeonghaseyo!", "Shalom!",
                "Sah-wahd-dee-kah! ", " Merhaba!", " Hujambo! ", " Olá! "
            )
            intent.putStringArrayListExtra("list", stringList)
            startActivity(intent)
        }
    }
}