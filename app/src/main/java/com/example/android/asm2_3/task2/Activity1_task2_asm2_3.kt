package com.example.android.asm2_3.task2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import com.example.fresher2022_kotlin_quanglv8b.R

class Activity1_task2_asm2_3 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_activity1_task2_asm23)
        val button = findViewById<Button>(R.id.button)
        val num1 = findViewById<EditText>(R.id.editTextNumberSigned)
        val num2 = findViewById<EditText>(R.id.editTextNumberSigned2)


        button.setOnClickListener{
            val so1 = num1.text.toString()
            val so2 = num2.text.toString()
            val intent: Intent = Intent(this,Activity2_task2_asm2_3::class.java)
            intent.putExtra("number1", so1)
            intent.putExtra("number2", so2)
            startActivity(intent)
        }

        val resul = intent.getStringExtra("sum")
        Log.i("TAG", resul.toString())
    }
}