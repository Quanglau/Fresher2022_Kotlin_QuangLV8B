package com.example.android.asm2_3.task3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.fresher2022_kotlin_quanglv8b.R

class Activity1_task3_asm2_3 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_activity1_task3_asm23)
        val button = findViewById<Button>(R.id.button)
        button.setOnClickListener{
            val intent: Intent = Intent(this,Activity2_task3_asm2_3::class.java)
            startActivity(intent)
//            val intent:Intent = Intent()
//            intent.setAction("test")
//            sendBroadcast(intent)
        }
    }
}