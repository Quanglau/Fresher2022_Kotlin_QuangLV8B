package com.example.android.asm2_3.task1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.fresher2022_kotlin_quanglv8b.R

class Activity2_task1_asm2_3 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_activity2_task1_asm23)
        val stringList = intent.getStringArrayListExtra("list")
        if (stringList != null) {
            for (i in stringList) Log.i("TAG", i)
        }
    }
}