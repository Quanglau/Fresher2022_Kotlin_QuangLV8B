package com.example.android.QuangLV8_BAD_FinalTest

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.fresher2022_kotlin_quanglv8b.R


class Fianal_MainActivity : AppCompatActivity(), View.OnClickListener {
    val fragmentManager = supportFragmentManager
    val buttonStart = findViewById<Button>(R.id.buttons)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.final_android_activity)
        buttonStart.setOnClickListener{

        }
    }
    val br: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val dialog = DialoFM()
            dialog.show(fragmentManager, "show dialog")
        }
    }

    override fun onPause() {
        super.onPause()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(br)
    }

    override fun onResume() {
        super.onResume()
        LocalBroadcastManager.getInstance(this).registerReceiver(br, IntentFilter())
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun startService() {
        intent = Intent(this, Service::class.java)
        startService(intent)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.buttons -> startService()
        }
    }

}
