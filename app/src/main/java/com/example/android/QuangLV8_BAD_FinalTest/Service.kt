package com.example.android.QuangLV8_BAD_FinalTest

import android.app.*
import android.app.Service
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.localbroadcastmanager.content.LocalBroadcastManager

class Service : Service() {

    override fun onBind(p0: Intent?): IBinder? {
        TODO("Not yet implemented")
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        createNotifyChannel()
        Thread {
            var sum = 0
            for (i in 0..10000) {
                sum = sum + i
            }
            Log.d("Tag", sum.toString())
            Thread.sleep(5000)
            sendBroadcast()

        }.start()
        val intent = Intent(this, Fianal_MainActivity::class.java)
        val intent1 = PendingIntent.getActivity(this, 0, intent, 0)
        val thongbao = Notification.Builder(this, "Xác nhận")
            .setContentTitle("Xác nhận")
            .setContentText("Xác nhận")
            .setContentIntent(intent1)
            .build()
        startForeground(1, thongbao)
        return super.onStartCommand(intent, flags, startId)
    }

    private fun createNotifyChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val thongbao1 = NotificationChannel("ma id", "ten", NotificationManager.IMPORTANCE_DEFAULT)
            val thongbao2 = getSystemService(NotificationManager::class.java)
            thongbao2.createNotificationChannel(thongbao1)
        }
    }
    private fun sendBroadcast() {
        val intent = Intent("message")
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }
}

